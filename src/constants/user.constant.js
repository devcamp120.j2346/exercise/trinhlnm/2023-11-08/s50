export const USER_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy danh sách user";

export const  USER_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy danh sách user";

export const  USER_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy danh sách user";

export const BUTTON_CHITIET_CLICKED = "Nút Chi tiết được ấn";

export const ON_CLOSE_MODAL = "Đóng modal user";

export const PAGE_CHANGE_PAGINATION = "Sự kiện thay đổi trang";

export const CHANGE_PAGINATION_LIMIT = "Sự kiện thay đổi số phân trang";