import { BUTTON_CHITIET_CLICKED, CHANGE_PAGINATION_LIMIT, ON_CLOSE_MODAL, PAGE_CHANGE_PAGINATION, USER_FETCH_ERROR, USER_FETCH_PENDING, USER_FETCH_SUCCESS } from "../constants/user.constant";

const initialState = {
    users: [],
    pending: false,
    openModal: false,
    clickedUser: {},
    limit: 10,
    noPage: 0,
    currentPage: 1
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_FETCH_PENDING:
            state.pending = true;
            break;
        case USER_FETCH_SUCCESS:
            state.pending = false;
            state.noPage = Math.ceil(action.totalUser / state.limit);
            state.users = action.data;
            break;
        case USER_FETCH_ERROR:
            break;
        case BUTTON_CHITIET_CLICKED:
            state.openModal = true;
            state.clickedUser = action.payload;
            break;
        case ON_CLOSE_MODAL:
            state.openModal = false;
            break;
        case PAGE_CHANGE_PAGINATION:
            state.currentPage = action.payload;
            break;
        case CHANGE_PAGINATION_LIMIT:
            state.limit = action.payload;
            break;
        default:
            break;
    }

    return { ...state };
}

export default userReducer;