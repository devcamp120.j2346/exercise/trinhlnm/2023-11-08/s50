import { Box, FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { changePaginationLimit, fetchUser } from "../actions/user.action";
import { useEffect } from "react";

const SelectNumberPage = () => {
    const dispatch = useDispatch();

    const { limit, currentPage } = useSelector(reduxData => reduxData.userReducer);

    const onLimitChange = (event) => {
        dispatch(changePaginationLimit(event.target.value));
    }

    useEffect(() => {
        dispatch(fetchUser(currentPage, limit));
    }, [limit]);

    return (
        <Box sx={{textAlign: "start"}}>
            <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id="demo-simple-select-standard-label">Phân trang</InputLabel>
                <Select
                    labelId="demo-simple-select-standard-label"
                    id="demo-simple-select-standard"
                    value={limit}
                    onChange={onLimitChange}
                    label="Phân trang"
                    sx={{textAlign: "center"}}
                >
                    <MenuItem value={5}>5</MenuItem>
                    <MenuItem value={10}>10</MenuItem>
                    <MenuItem value={25}>25</MenuItem>
                    <MenuItem value={50}>50</MenuItem>
                </Select>
            </FormControl>
        </Box>
    );
}

export default SelectNumberPage;