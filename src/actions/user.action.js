import { BUTTON_CHITIET_CLICKED, CHANGE_PAGINATION_LIMIT, ON_CLOSE_MODAL, PAGE_CHANGE_PAGINATION, USER_FETCH_ERROR, USER_FETCH_PENDING, USER_FETCH_SUCCESS } from "../constants/user.constant";

export const fetchUser = (page, limit) => {
    return async(dispatch) => {
        try {
            var requestOptions = {
                method: "GET"
            }

            await dispatch({
                type: USER_FETCH_PENDING
            });

            const resUser = await fetch("http://203.171.20.210:8080/devcamp-register-java-api/users", requestOptions);

            const dataUser = await resUser.json();

            //phân trang
            const params = new URLSearchParams({
                page: page - 1,
                size: limit
            });

            const resPaginationUser = await fetch("http://203.171.20.210:8080/devcamp-register-java-api/user-list-pagination?" + params.toString(), requestOptions);

            const dataPaginationUser = await resPaginationUser.json();

            console.log("http://203.171.20.210:8080/devcamp-register-java-api/user-list-pagination?" + params.toString());

            return dispatch({
                type: USER_FETCH_SUCCESS,
                totalUser: dataUser.length,
                data: dataPaginationUser
            });
        } catch (error) {
            return dispatch({
                type: USER_FETCH_ERROR,
                error: error
            });
        }
    }
}

export const btnChitietClickedAction = (user) => {
    return {
        type: BUTTON_CHITIET_CLICKED,
        payload: user
    };
}

export const onClose = () => {
    return {
        type: ON_CLOSE_MODAL
    };
}

export const pageChangePagination = (page) => {
    return {
        type: PAGE_CHANGE_PAGINATION,
        payload: page
    };
}

export const changePaginationLimit = (limit) => {
    return {
        type: CHANGE_PAGINATION_LIMIT,
        payload: limit
    };
}