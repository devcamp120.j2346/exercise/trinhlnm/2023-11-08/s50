import { Button, CircularProgress, Container, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, styled, tableCellClasses } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btnChitietClickedAction, fetchUser, pageChangePagination } from "../actions/user.action";
import UserModal from "./UserModal";
import SelectNumberPage from "./SelectNumberPage";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        fontWeight: "bold",
        border: "1px solid rgb(203, 203, 203)"
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
        border: "1px solid rgb(203, 203, 203)"
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    }
}));

const UserList = () => {
    const dispatch = useDispatch();

    const { users, pending, openModal, limit, noPage, currentPage } = useSelector(reduxData => reduxData.userReducer);

    useEffect(() => {
        dispatch(fetchUser(currentPage, limit));
    }, [currentPage]);

    const onChiTietBtnClicked = (user) => {
        dispatch(btnChitietClickedAction(user));
    }

    const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value));
    }

    return (
        <>
            <Container style={{ textAlign: "center" }}>
                <Typography variant="h4" style={{ margin: "1rem", marginBottom: "0rem" }}>Danh sách đăng ký</Typography>

                <SelectNumberPage/>

                {
                    pending ? <CircularProgress />
                        : <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell>Mã người dùng</StyledTableCell>
                                        <StyledTableCell>Firstname</StyledTableCell>
                                        <StyledTableCell>Lastname</StyledTableCell>
                                        <StyledTableCell>Country</StyledTableCell>
                                        <StyledTableCell>Subject</StyledTableCell>
                                        <StyledTableCell>Customer Type</StyledTableCell>
                                        <StyledTableCell>Register Status</StyledTableCell>
                                        <StyledTableCell>Action</StyledTableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {users.map((e, i) => (
                                        <StyledTableRow key={i}>
                                            <StyledTableCell component="th" scope="row">
                                                {e.id}
                                            </StyledTableCell>
                                            <StyledTableCell>{e.firstname}</StyledTableCell>
                                            <StyledTableCell>{e.lastname}</StyledTableCell>
                                            <StyledTableCell>{e.country}</StyledTableCell>
                                            <StyledTableCell>{e.subject}</StyledTableCell>
                                            <StyledTableCell>{e.customerType}</StyledTableCell>
                                            <StyledTableCell>{e.registerStatus}</StyledTableCell>
                                            <StyledTableCell>
                                                <Button size="small" variant="contained" style={{ textTransform: "capitalize" }} onClick={() => onChiTietBtnClicked(e)}>Chi tiết</Button>
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                }

                <Pagination count={noPage} variant="outlined" shape="rounded" style={{ marginTop: "2rem", display: "flex", justifyContent: "center" }} onChange={onChangePagination}/>
            </Container>

            <UserModal open={openModal} />
        </>
    );
}

export default UserList;